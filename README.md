# Bower Libraries

## Description

A Drupal module to automatically add `bower` components in the active theme to the Libraries API registry.

## Usage

Install and enable this module, and then install some components in your active theme using Bower, like so:

    bower install backbone
    
This module will recognize the components and add them to the list of available libraries. If you are using `drush`,
you can check that the components have been recognized using `drush libraries-list`.

